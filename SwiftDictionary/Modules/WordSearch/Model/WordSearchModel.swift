//
//  WordSearchModel.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/28/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit
import ReachabilitySwift

extension String {
    func isCyrillic() -> Bool {
        // If String contains both english and cyrillic symbols it will be treated like nonEnglish
        let set = NSCharacterSet(charactersIn:"абвгдеёжзийклмнопрстуфхчцшщьъэюяіїґєАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЧЦШЩЬЪЭЮЯІЇҐЄ")
        return self.rangeOfCharacter(from: set as CharacterSet) != nil
        }
}

@objc protocol WordSearchModelDelegate {
    func modelDidBeginRemoteTranslation(model: WordSearchModel?)
    func modelDidEndRemoteTranslation(model: WordSearchModel?)
    func modelDidUpdateData(model: WordSearchModel?, willContinueSearch:Bool)
    func modelDidStopSearchBecauseOfNoInternetConnection(model: WordSearchModel?)
}

class WordSearchModel: NSObject {
    
    weak var delegate: WordSearchModelDelegate?
    private var service = WordSearchService()
    private var translations:[ManagedTranslation] = []
    private var isFromEnglish = true
    
    func items() -> [ManagedTranslation] {
        return translations
    }
    
    func searchFor(text: String) {
        let predicate = NSPredicate(format: "originalWord BEGINSWITH[cd] %@ || translatedWord BEGINSWITH[cd] %@", text, text)
        
        guard let result = ManagedTranslation.mr_findAllSorted(by: "originalWord", ascending: true, with: predicate) else {
            print("Failed to fetch results for \(text)")
            delegate?.modelDidUpdateData(model: self, willContinueSearch: false)
            return;
        }
        if result.count > 0 {
            // Update data to show
            translations = result as! [ManagedTranslation]
            
            delegate?.modelDidUpdateData(model: self, willContinueSearch: false)
        } else {
            translations.removeAll()
            delegate?.modelDidUpdateData(model: self, willContinueSearch: true)
            // Translate text by service
            translate(text: text)
        }
    }
    
    func cancelSearching() {
        service.cancelAllTasks()
        delegate?.modelDidEndRemoteTranslation(model: self)
    }
    
    private func translate(text: String) {
        // Check Internte connection
        guard isInternetReachable() == true else {
            delegate?.modelDidStopSearchBecauseOfNoInternetConnection(model: self)
            return
        }
        // Notify delegate
        DispatchQueue.main.async {
            self.delegate?.modelDidBeginRemoteTranslation(model: self)
        }
        
        // Detect language
        let toEnglish = text.isCyrillic()
        
        // Start remote translation
        self.service.translate(text: text, toEnglish: toEnglish, successHandler: {[weak self] (result: [Translation]?) in
            var managedTranslations = [ManagedTranslation]()
            for translation: Translation in result! {
                // Save translation to CoreData
                let managedTranslation = translation.convertToManagedTranslation()
                // Add to array
                managedTranslations.append(managedTranslation)
            }
            self?.translations = managedTranslations
            // Notify delegate
            DispatchQueue.main.async {
                self?.delegate?.modelDidUpdateData(model: self, willContinueSearch: false)
                self?.delegate?.modelDidEndRemoteTranslation(model: self)
            }
        }, failureHandler: {[weak self] (error: Error) in
            //Clear data
            self?.translations.removeAll()
            // Notify delegate
            DispatchQueue.main.async {
                self?.delegate?.modelDidUpdateData(model: self, willContinueSearch: false)
                self?.delegate?.modelDidEndRemoteTranslation(model: self)
            }
        })
    }
    
    func isInternetReachable() -> Bool {
        return (Reachability.init()?.isReachable == true)
    }
}
