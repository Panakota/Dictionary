//
//  WordSearchService.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/28/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit
import AFNetworking

class WordSearchService: NSObject {
    
    let wordSearchServiceErrorDomain = "com.anastasiia.SwiftDictionary.WordSearchService"
    let manager = AFURLSessionManager(sessionConfiguration: .default)
    
    enum WordSearchServiceError : Int {
        case stringEscapingError = -1
        case urlCreationError = -2
    }
    
    
    func translate(text: String, toEnglish: Bool, successHandler:@escaping (_ items: [Translation]?) -> Void, failureHandler: @escaping (_ error: Error) -> Void) {
        guard let escapedText = text.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else {
            let error = NSError.init(domain: wordSearchServiceErrorDomain, code: WordSearchServiceError.stringEscapingError.rawValue, userInfo: nil)
            failureHandler(error)
            return
        }
        
        let langpair = toEnglish ? "ru%7Cen" : "en%7Cru"
        let urlString = "https://api.mymemory.translated.net/get?q=" + escapedText + "&langpair=" + langpair

        guard let url = NSURL(string: urlString) else {
            let error = NSError.init(domain: wordSearchServiceErrorDomain, code: WordSearchServiceError.urlCreationError.rawValue, userInfo: nil)
            failureHandler(error)
            return
        }
        let request = NSURLRequest(url: url as URL)
        // Cancel all previously created tasks becauses the new one will be created now
        cancelAllTasks()
        let dataTask = manager.dataTask(with: request as URLRequest) { (response: URLResponse, responseObject: Any?, error: Error?) in
            if let error = error {
                let error = error as NSError
                // Check if task was cancelled
                if error.code == NSURLErrorCancelled {
                    print("Cancelled")
                    return
                }
                
                print("Error: \(error.localizedDescription)")
                failureHandler(error as Error)
            } else {
                print("Success")
                let parsedResult = self.parseTranslation(with: responseObject, isToEnglish:toEnglish)
                successHandler(parsedResult)
            }
        };
        dataTask.resume()
    }
    
    func cancelAllTasks() {
        for dataTask in manager.dataTasks {
            dataTask.cancel()
        }
    }
    
    private func parseTranslation(with response: Any, isToEnglish: Bool) -> [Translation]? {
        guard let response = response as? NSDictionary else {
            return nil
        }
        
        guard let matches = response["matches"] as? [AnyObject] else {
            return nil
        }
        var result = [Translation]()
        for match in matches {
            if let input = match["segment"] as? String, let output = match["translation"] as? String {
                let translation = Translation()
                translation.originalWord = input
                translation.translatedWord = output
                translation.isToEnglish = isToEnglish
                
                result.append(translation)
            }
        }
        return result
    }
}
