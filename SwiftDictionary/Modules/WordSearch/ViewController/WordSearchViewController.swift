//
//  WordSearchViewController.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/28/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit
import Toast_Swift

class WordSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var rootNavigationController: UINavigationController?
    var model = WordSearchModel()
    var searchBar: UISearchBar?
    var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    let cellIdentifier = "WordSearchCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
        tableView.register(UINib.init(nibName: "TranslationTableViewCell", bundle: nil), forCellReuseIdentifier:cellIdentifier)
        tableView.tableFooterView = UIView()
        setupActivityIndicator()
        model.delegate = self
    }
    
    func setupActivityIndicator() {
        if let searchBar = searchBar {
            activityIndicator.frame = CGRect(x: 5.0, y: 5.0, width: 30.0, height: 30.0)
            activityIndicator.autoresizingMask = .flexibleLeftMargin
            activityIndicator.hidesWhenStopped = true
            searchBar.addSubview(activityIndicator)
        }
    }
}

extension WordSearchViewController: WordSearchModelDelegate {
    func modelDidBeginRemoteTranslation(model: WordSearchModel?) {
        activityIndicator.startAnimating()
    }
    
    func modelDidEndRemoteTranslation(model: WordSearchModel?) {
        activityIndicator.stopAnimating()
    }
    
    func modelDidUpdateData(model: WordSearchModel?, willContinueSearch:Bool) {
        tableView.reloadData()
        if model?.items().count == 0 && willContinueSearch == false {
            self.view.makeToast("No results found", duration: 2.0, position: .center)
        }
    }
    
    func modelDidStopSearchBecauseOfNoInternetConnection(model: WordSearchModel?) {
        self.view.makeToast("No Internet connection", duration: 2.0, position: .center)
    }
}

extension WordSearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if text.characters.count > 0 {
                model.searchFor(text: text)
            }
        }
    }
}

extension WordSearchViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        model.cancelSearching()
    }
}

extension WordSearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.items().count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TranslationTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TranslationTableViewCell
        
        let translation = model.items()[indexPath.row]
        cell.configure(withOriginalWord: translation.originalWord!, translatedWord: translation.translatedWord!, isLocal: true)
        
        return cell
    }
}

extension WordSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let managedTranslation = model.items()[indexPath.row] as ManagedTranslation? {
            let translationInfoViewController = TranslationInfoViewController(nibName: "TranslationInfoViewController", bundle: nil)
            translationInfoViewController.translation = managedTranslation
            let nav = UINavigationController(rootViewController: translationInfoViewController)
            nav.navigationBar.isTranslucent = false
            rootNavigationController?.present(nav, animated: true, completion:nil)
        }
    }
}
