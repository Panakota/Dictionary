//
//  TranslationInfoViewController.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/29/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit

class TranslationInfoViewController: UIViewController {
    
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var originalWordLabel: UILabel!
    @IBOutlet weak var translatedWordLabel: UILabel!
    var translation: ManagedTranslation?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Translation Details"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupCancelButton()
        
        if let translation = translation {
            let fromLanguage = translation.isToEnglish ? "ru" : "en"
            let toLanguage = translation.isToEnglish ? "en" : "ru"
            fromLabel.text = "From (\(fromLanguage)):"
            toLabel.text = "To (\(toLanguage)):"
            originalWordLabel.text = translation.originalWord
            translatedWordLabel.text = translation.translatedWord
        }
    }
    
    func setupCancelButton() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonAction(_:)))
        navigationItem.setLeftBarButton(cancelButton, animated: false)
    }
    
    func cancelButtonAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}
