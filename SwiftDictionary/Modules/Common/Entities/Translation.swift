//
//  Translation.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/28/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit
import MagicalRecord

class Translation: NSObject {
    var originalWord: String?
    var translatedWord: String?
    var isToEnglish: Bool
    
    override init() {
        originalWord = nil
        translatedWord = nil
        isToEnglish = false
        
        super.init()
    }
    
    func convertToManagedTranslation() -> ManagedTranslation {
        let managedTranslation = ManagedTranslation.mr_createEntity() as ManagedTranslation!
        managedTranslation?.originalWord = originalWord
        managedTranslation?.translatedWord = translatedWord
        managedTranslation?.isToEnglish = isToEnglish
        managedTranslation?.dateCreated = NSDate()
        
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        
        return managedTranslation!
    }
}
