//
//  ManagedTranslation+CoreDataProperties.swift
//  
//
//  Created by Anastasiia on 12/29/16.
//
//

import Foundation
import CoreData


extension ManagedTranslation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedTranslation> {
        return NSFetchRequest<ManagedTranslation>(entityName: "ManagedTranslation");
    }

    @NSManaged public var dateCreated: NSDate?
    @NSManaged public var isToEnglish: Bool
    @NSManaged public var originalWord: String?
    @NSManaged public var translatedWord: String?

}
