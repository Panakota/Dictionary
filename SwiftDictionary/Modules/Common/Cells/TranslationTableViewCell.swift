//
//  TranslationTableViewCell.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/28/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit

class TranslationTableViewCell: UITableViewCell {

    @IBOutlet weak var translationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(withOriginalWord: String, translatedWord: String, isLocal: Bool) {
        
        self.translationLabel.text = "\(withOriginalWord) — \(translatedWord)"
    }
    
}
