//
//  TranslatedWordsListModel.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/27/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit
import MagicalRecord

@objc protocol TranslatedWordsListModelDelegate {
    func modelDidUpdateData(model: TranslatedWordsListModel)
}

class TranslatedWordsListModel: NSObject {
    
    weak var delegate: TranslatedWordsListModelDelegate?
    private var translations:[ManagedTranslation] = []
    
    func reloadItems() {
        translations = ManagedTranslation.mr_findAllSorted(by: "dateCreated", ascending: false) as! [ManagedTranslation]
        delegate?.modelDidUpdateData(model: self)
    }
    
    func items() -> [ManagedTranslation] {
        return translations
    }
    
    func clearAllLocalData() {
        ManagedTranslation.mr_truncateAll()
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
        reloadItems()
    }
}
