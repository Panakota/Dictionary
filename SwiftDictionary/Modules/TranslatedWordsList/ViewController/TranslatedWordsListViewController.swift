//
//  TranslatedWordsListViewController.swift
//  SwiftDictionary
//
//  Created by Anastasiia on 12/27/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

import UIKit

class TranslatedWordsListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var model = TranslatedWordsListModel()
    
    var searchController: UISearchController = UISearchController(searchResultsController: nil)
    
    let cellIdentifier = "TranslatedWordsListCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // because of this http://stackoverflow.com/questions/31844123/uisearchcontroller-changing-status-bar-color-on-invocation
        extendedLayoutIncludesOpaqueBars = true
        
        tableView.register(UINib.init(nibName: "TranslationTableViewCell", bundle: nil), forCellReuseIdentifier:cellIdentifier)
        tableView.tableFooterView = UIView()
        self.title = "Translations History"
        model.delegate = self
        setupSearchController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.model.reloadItems()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        model.clearAllLocalData()
    }
    
    func setupSearchController() {
        let resultViewController = WordSearchViewController(nibName: "WordSearchViewController", bundle: nil)
        searchController = UISearchController(searchResultsController: resultViewController)
        searchController.delegate = self
        searchController.searchBar.delegate = resultViewController
        searchController.searchResultsUpdater = resultViewController
        searchController.dimsBackgroundDuringPresentation = true
        searchController.hidesNavigationBarDuringPresentation = true
        
        resultViewController.searchBar = searchController.searchBar
        resultViewController.rootNavigationController = navigationController
        
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
}

extension TranslatedWordsListViewController: TranslatedWordsListModelDelegate {
    func modelDidUpdateData(model: TranslatedWordsListModel) {
        tableView.reloadData()
        if model.items().count == 0 {
            self.view.makeToast("No translation history yet", duration: 2.0, position: .center)
        }
    }
}

extension TranslatedWordsListViewController: UISearchControllerDelegate {
    func willDismissSearchController(_ searchController: UISearchController) {
        model.reloadItems()
    }
}

extension TranslatedWordsListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.items().count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TranslationTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TranslationTableViewCell
        
        let translation = model.items()[indexPath.row]
        cell.configure(withOriginalWord: translation.originalWord!, translatedWord: translation.translatedWord!, isLocal: true)
        
        return cell
    }
}

extension TranslatedWordsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let managedTranslation = model.items()[indexPath.row] as ManagedTranslation? {
            let translationInfoViewController = TranslationInfoViewController(nibName: "TranslationInfoViewController", bundle: nil)
            translationInfoViewController.translation = managedTranslation
            let nav = UINavigationController(rootViewController: translationInfoViewController)
            nav.navigationBar.isTranslucent = false
            navigationController?.present(nav, animated: true, completion:nil)
        }
    }
}
